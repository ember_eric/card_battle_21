﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTurnManager : Photon.PunBehaviour {
	public static CardTurnManager singleton = null;

	#region  TurnTime
    public float TurnDuration = 20f;

	public float ElapsedTimeInTurn
	{
		get { return ((float)(PhotonNetwork.ServerTimestamp - PhotonNetwork.room.GetTurnStart()))/1000.0f; }
	}

	public float RemainingSecondsInTurn
	{
		get { return Mathf.Max(0f,this.TurnDuration - this.ElapsedTimeInTurn); }
	}
	#endregion

	#region EvCode
	// 1 Game has N Rounds, 1 Round has N Turns, 1 Turn has N Actions.
	public const byte eventOffset = 0;
	public const byte EvStartGame = 1 + eventOffset;
	public const byte EvEndGame = 2 + eventOffset;
	public const byte EvStartRound = 3 + eventOffset;
	public const byte EvEndRound = 4 + eventOffset;
	public const byte EvEndTurn = 5 + eventOffset;
	public const byte EvAction = 6 + eventOffset;	
	public const byte EvPass = 7 + eventOffset;
	#endregion

	public ICardTurnManagerListener cardTurnManagerListener = null;

	int firstPlayerID = -1;
	int currentPlayerID = -1;
	bool playerPassed = false;
	bool otherPassed = false;

	void Awake()
    {
		if(singleton) {
			Destroy(this.gameObject);
		}
		singleton = this;

        PhotonNetwork.OnEventCall = OnEvent;
    }

	public bool IsMyTurn() {
		if(currentPlayerID == PhotonNetwork.player.ID) {
			return true;
		}
		return false;
	}

	public void SendAction(object info, byte evCode) {
		PhotonNetwork.RaiseEvent(evCode, info, true, null);
		OnEvent(evCode,info,PhotonNetwork.player.ID);
	}

	protected void OnEvent(byte eventCode, object content, int senderId) {
		PhotonPlayer sender = PhotonPlayer.Find(senderId);
		switch(eventCode) {
			case EvStartGame: {
				this.StartGame(content);
				cardTurnManagerListener.OnStartGame(content);
				break;
			}
			case EvEndGame: {
				cardTurnManagerListener.OnEndGame();
				break;
			}
			case EvStartRound: {
				cardTurnManagerListener.OnStartRound();
				break;
			}
			case EvEndRound: {
				cardTurnManagerListener.OnEndRound();
				break;
			}
			case EvEndTurn: {
				this.EndCurrentTurn();
				cardTurnManagerListener.OnNextTurn();
				break;
			}
			case EvAction: {
				cardTurnManagerListener.OnPlayerAction(content);
				break;
			}
			case EvPass: {
				if(senderId == PhotonNetwork.player.ID) {
					playerPassed = true;
					cardTurnManagerListener.OnNextTurn();
				}
				else {
					otherPassed = true;
					cardTurnManagerListener.OnNextTurn();
				}
				break;
			}
		}
	}

	protected void StartGame(object content) {
		int playerID = (int)cardTurnManagerListener.GetObjectFromDict(content, (int)CardGameManager.InfoKey.firstPlayer);
		firstPlayerID = playerID;
		currentPlayerID = playerID;
	}

	protected void EndCurrentTurn() {
		if(currentPlayerID == PhotonNetwork.player.ID) {
			currentPlayerID = PhotonNetwork.playerList[0].ID;
		}
		else {
			currentPlayerID = PhotonNetwork.player.ID;
		}
	}
}

public interface ICardTurnManagerListener {
	void OnStartGame(object dict);
	void OnEndGame();
	void OnStartRound();
	void OnEndRound();
	void OnNextTurn();
	void OnPlayerAction(object dict);
	object GetObjectFromDict (object dict, int key);
}
