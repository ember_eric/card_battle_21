﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGameManager : MonoBehaviour, ICardTurnManagerListener {
	public List<int> cardPool;

	public GameObject cardPrefab;
	public GameObject myCardSlot;
	public GameObject otherCardSlot;
	public List<GameObject> myCards;
	public List<GameObject> otherCards;

	[Header("UI")]

	public Button drawCardButton;
	public Button endTurnButton;
	public Text endTurnButtonText;
	public Text playerName;
	public Text otherPlayerName;
	public Text myPanelText;
	public Text otherPanelText;
	public Text myManaText;
	public Text otherManaText;

	public enum Action {
		None = 0,
		startRoundDraw,
		drawCard,
		filpCard,
		pass
	}

	public enum InfoKey {
		None = 0,
		action,
		firstPlayer,
		p1card0, // firstPlayer = p1, for initial draw
		p1card1, // for initial draw
		p2card0, // for initial draw
		p2card1, // for initial draw
		cardValue
	}

	int p1ID = -1;
	int myCardSlotIndex = 0;
	int otherCardSlotIndex = 0;
	bool hasDoneAnyAction = false;
	bool isPassed = false;
	
	void Start () {
		CardTurnManager.singleton.cardTurnManagerListener = this;
		UpdatePlayerName();
		cardPool = new List<int>();
		for(int i = 1 ; i < 11 ; i++) {
			cardPool.Add(i);
		}

		if(PhotonNetwork.isMasterClient) {
			PickFirstPlayer();
		}
	}

	public void PickFirstPlayer() {
		Debug.Log("[GameManager] - Master client pick first player");
		int rand = Random.Range(0,2);
		Dictionary<int,object> infoDict = new Dictionary<int,object>();
		int playerID = PhotonNetwork.playerList[rand].ID;
		infoDict.Add((int)InfoKey.firstPlayer, playerID);
		CardTurnManager.singleton.SendAction(infoDict, CardTurnManager.EvStartGame);
	}

	public void DrawFirstTwoCard() {
		Debug.Log("[GameManager] - Master client draw first two cards");
		Dictionary<int,object> infoDict = new Dictionary<int,object>();
		for(int i = 0 ; i < 4 ; i++) {
			int rand = Random.Range(0,cardPool.Count);
			int cardValue = cardPool[rand];
			cardPool.RemoveAt(rand);
			//Draw cards for both player
			switch(i) {
				case 0:
					infoDict.Add((int)InfoKey.p1card0, cardValue);
					break;
				case 1:
					infoDict.Add((int)InfoKey.p2card0, cardValue);
					break;
				case 2:
					infoDict.Add((int)InfoKey.p1card1, cardValue);
					break;
				case 3:
					infoDict.Add((int)InfoKey.p2card1, cardValue);
					break;
			}
		}
		infoDict.Add((int)InfoKey.action, (int)Action.startRoundDraw);
		CardTurnManager.singleton.SendAction(infoDict, CardTurnManager.EvAction);
	}

	public void AddCard(bool isCovered, bool isPositive, int value, bool isMyCard) {
		SyncCardPool(value);

		Transform cardSlot;
		if(isMyCard) {
			string slotName = "CardSlot" + myCardSlotIndex;
			cardSlot = myCardSlot.transform.Find(slotName);
			myCardSlotIndex++;
		}
		else {
			string slotName = "CardSlot" + otherCardSlotIndex;
			cardSlot = otherCardSlot.transform.Find(slotName);
			otherCardSlotIndex++;
		}
		GameObject cardObj = Instantiate(cardPrefab, cardSlot) as GameObject;
		cardObj.GetComponent<Card>().CreateCard(isCovered, isPositive, value);
	}

	public void DrawCardOnClick() {
		int rand = Random.Range(0,cardPool.Count);
		int cardValue = cardPool[rand];
		cardPool.RemoveAt(rand);

		Dictionary<int,object> infoDict = new Dictionary<int,object>();
		infoDict.Add((int)InfoKey.cardValue, cardValue);
		infoDict.Add((int)InfoKey.action, (int)Action.drawCard);
		CardTurnManager.singleton.SendAction(infoDict, CardTurnManager.EvAction);
	}

	public void EndOnClick() {
		// Pass if no action performed. In Pass status the player will no longer able to perform any action until next round.
		if(hasDoneAnyAction) {
			CardTurnManager.singleton.SendAction(null, CardTurnManager.EvEndTurn);
		}
		else {
			this.Pass();
		}
	}

	public void Pass() {
		isPassed = true;
		Dictionary<int,object> infoDict = new Dictionary<int,object>();
		infoDict.Add((int)InfoKey.action, (int)Action.pass);
		CardTurnManager.singleton.SendAction(infoDict, CardTurnManager.EvAction);
	}

	public void SyncCardPool(int removeValue) {
		cardPool.Remove(removeValue);
	}

	#region UI Update
	public void UpdatePlayerName() {
		playerName.text = PhotonNetwork.player.NickName;
		otherPlayerName.text = PhotonNetwork.otherPlayers[0].NickName;
	}
	public void UpdatePlayerActionButtons() {
		if(CardTurnManager.singleton.IsMyTurn()) {
			drawCardButton.interactable = true;
			endTurnButton.interactable = true;
		}
		else {
			drawCardButton.interactable = false;
			endTurnButton.interactable = false;
		}
	}

	public void UpdateEndButtonText() {

	}

	public void UpdatePlayerPanelText() {
		if(CardTurnManager.singleton.IsMyTurn()) {
			myPanelText.text = "Your turn";
			otherPanelText.text = "Waiting ...";
		}
		else {
			myPanelText.text = "Waiting ...";
			otherPanelText.text = "Enemy turn";
		}
	}

	public void UpdateMana() {

	}
	#endregion


	#region CardTurnManagerListener
	public void OnStartGame(object dict) {
		Debug.Log("[GameManager] - User on start game, recieved p1ID");
		p1ID = (int)GetObjectFromDict(dict, (int)InfoKey.firstPlayer);
		UpdatePlayerActionButtons();
		UpdatePlayerPanelText();
		if(PhotonNetwork.isMasterClient) {
			DrawFirstTwoCard();
		}
	}

	public void OnEndGame() {

	}

	public void OnStartRound() {

	}

	public void OnEndRound() {

	}

	public void OnNextTurn() {
		if(isPassed) {
			this.Pass();
		}
		else {
			Debug.Log("[GameManager] - Action: endTurn.");
			UpdatePlayerActionButtons();
			UpdatePlayerPanelText();
		}
	}

	public void OnPlayerAction(object dict) {
		int action = (int)GetObjectFromDict(dict, (int)InfoKey.action);
		switch(action) {
			case (int)Action.startRoundDraw: {
				Debug.Log("[GameManager] - Action: startRoundDraw. Receive drawed cards");
				int myCard0 = -1;
				int myCard1 = -1;
				int otherCard0 = -1;
				int otherCard1 = -1;
				if(PhotonNetwork.player.ID == p1ID) {
					myCard0 = (int)GetObjectFromDict(dict, (int)InfoKey.p1card0);
					myCard1 = (int)GetObjectFromDict(dict, (int)InfoKey.p1card1);
					otherCard0 = (int)GetObjectFromDict(dict, (int)InfoKey.p2card0);
					otherCard1 = (int)GetObjectFromDict(dict, (int)InfoKey.p2card1);
				}
				else {
					myCard0 = (int)GetObjectFromDict(dict, (int)InfoKey.p2card0);
					myCard1 = (int)GetObjectFromDict(dict, (int)InfoKey.p2card1);
					otherCard0 = (int)GetObjectFromDict(dict, (int)InfoKey.p1card0);
					otherCard1 = (int)GetObjectFromDict(dict, (int)InfoKey.p1card1);
				}

				AddCard(false,true,myCard0,true);
				AddCard(false,true,myCard1,true);
				AddCard(true,true,otherCard0,false);
				AddCard(false,true,otherCard1,false);
				break;
			}
			case (int)Action.drawCard: {
				int cardValue = (int)GetObjectFromDict(dict, (int)InfoKey.cardValue);
				if(CardTurnManager.singleton.IsMyTurn()) {
					Debug.Log("[GameManager] - Action: Draw card. I drawed a card");
					AddCard(false,true,cardValue,true);
				}
				else {
					Debug.Log("[GameManager] - Action: Draw card. Other drawed a card");
					AddCard(false,true,cardValue,false);
				}
				break;
			}
			case (int)Action.filpCard: {
				break;
			}
			case (int)Action.pass: {
				break;
			}
		}
	}

	//Helper function
	public object GetObjectFromDict (object dict, int key) {
		Dictionary<int,object> myDict = dict as Dictionary<int,object>;
		object obj;
		myDict.TryGetValue(key, out obj);
		return obj;
	}
	#endregion
}
