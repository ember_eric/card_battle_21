﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class PlayerNameInputField : MonoBehaviour {
	static string playerNamePrefKey = "PlayerName";
	// Use this for initialization
	void Start () {
		string defaultName = "Player";
		InputField _inputField = this.GetComponent<InputField>();
		if(_inputField != null) {
			if(PlayerPrefs.HasKey(playerNamePrefKey)) {
				defaultName = PlayerPrefs.GetString(playerNamePrefKey);
				_inputField.text = defaultName;
				Debug.Log("Has Name : " + defaultName);
			}
			else {
				Debug.Log("Has No Name");
			}
		}

		PhotonNetwork.playerName = defaultName;
	}

	public void SetPlayerName() {
		InputField _inputField = this.GetComponent<InputField>();
		PlayerPrefs.SetString(playerNamePrefKey, _inputField.text);
		PlayerPrefs.Save();
		PhotonNetwork.playerName = _inputField.text + " ";
	}
}
