﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Launcher : Photon.PunBehaviour {
	public GameObject logPanel;
	public Text log;
	public GameObject lobbyPanel;
	public GameObject PlayButtonObj;
	string _gameVersion = "1";
	
	private void Awake() {
		PhotonNetwork.autoJoinLobby = false;
		PhotonNetwork.automaticallySyncScene = true;
	}

	public void PlayBtnOnClick() {
		PhotonNetwork.ConnectUsingSettings(_gameVersion);
		PlayButtonObj.SetActive(false);
		logPanel.SetActive(true);
		log.text = "Connecting ...";
	}

	public void MatchMakingOnClick() {
		PhotonNetwork.JoinRandomRoom();
		log.text = "Finding match ...";
		logPanel.SetActive(true);
		lobbyPanel.SetActive(false);
	}

	public override void OnConnectedToMaster() {
		logPanel.SetActive(false);
		lobbyPanel.SetActive(true);
	}

	public override void OnPhotonRandomJoinFailed(object[] codeAndMsg) {
		Debug.Log("Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, create a new one.");
		PhotonNetwork.CreateRoom(null, new RoomOptions{MaxPlayers = 2, PlayerTtl = 60000}, null);
		log.text = "No room available, create a new one.";
	}

	public override void OnJoinedRoom() {
   		Debug.Log("Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
		
		if(PhotonNetwork.room.PlayerCount < 2) {
			log.text = "["+ PhotonNetwork.room.Name +"] \n Waiting for an opponent ...";
			PhotonNetwork.room.IsOpen = true;
			PhotonNetwork.room.IsVisible = true;
		}
	}

	public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer) {
		log.text = "[" + newPlayer.NickName + "] has joined your room!";
		LoadGame();
	}

	public void LoadGame() {
		if(PhotonNetwork.isMasterClient) {
			PhotonNetwork.LoadLevel(1);
		}
	}
}
