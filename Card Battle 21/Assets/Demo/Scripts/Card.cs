﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Card : MonoBehaviour {
	public bool cardCovered = true;
	public bool cardPositive = true;
	public int cardNo = 0;
	public GameObject cardHolder;
	public Animator cardAnimator;
	public Canvas cardFront;
	public Canvas cardBack;
	
	enum CardAnimState {BACK, FRONT, FILP_TO_FRONT, FILP_TO_BACK};

	static string AnimStateToString(CardAnimState state) {
		string stateString = "";
		if(state == CardAnimState.BACK) {
			stateString = "CardIdleCovered";
		}
		else if(state == CardAnimState.FRONT) {
			stateString = "CardIdle";
		}
		else if(state == CardAnimState.FILP_TO_BACK) {
			stateString = "CardFlipToBack";
		}
		else if(state == CardAnimState.FILP_TO_FRONT) {
			stateString = "CardFliptoFront";
		}
		return stateString;
	}

	private void Start() {

	}
	
	public void CreateCard(bool isCovered, bool isPositive, int cardNo) {
		SetCardInitAnimState(isCovered);
		SetCardIsPositive(isPositive);
		SetCardNumber(cardNo);
	}

	public void SetCardNumber(int number) {
		cardNo = number;
		Text numText = cardFront.transform.Find("Number").GetComponent<Text>();
		numText.text = number.ToString();
	}

	public void SetCardIsPositive(bool isPositive) {
		Text signText = cardFront.transform.Find("Sign").GetComponent<Text>();
		if(isPositive) {
			signText.text = "+";
		}
		else {
			signText.text = "-";
		}
		cardPositive = isPositive;
	}

	void SetCardInitAnimState(bool isCovered) {
		if(isCovered) {
			cardAnimator.Play(AnimStateToString(CardAnimState.BACK));
		}
		else {
			cardAnimator.Play(AnimStateToString(CardAnimState.FRONT));
		}
		cardCovered = isCovered;
	}
}
